// middlewares/cors.js
import NextCors from 'nextjs-cors';

async function cors(req, res) {
    await NextCors(req, res, {
        // 选项
        methods: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'],
        origin: "*",  // 根据您的安全策略调整
        optionsSuccessStatus: 200,
    });
}

export default cors;
