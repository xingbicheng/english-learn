import React, { useEffect, useState, useMemo, useCallback } from 'react';
import { useRouter } from 'next/router';
import { Box, Typography, TextField, Button, Checkbox, Modal } from '@mui/material';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import EditNoteIcon from '@mui/icons-material/EditNote';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import { toast } from 'react-toastify';
import { request } from "../utils/request";
import { encrypt, decrypt, htmlString, DOMAIN } from "../utils/utils";
import MyMarkdownEditor from '../components/MyMarkdownEditor';
import './collections.css'

// 在你的 Next.js 页面中
export async function getServerSideProps () {
    // 获取数据
    const data = await request(`${DOMAIN}/api/collections/all`, 'GET')
    // 通过 props 将数据传递给页面
    return { props: { data } };
}

function Collections ({ data = [] }) {
    const router = useRouter();
    const [collections, setCollections] = useState(data);
    const [words, setWords] = useState((collections[0] || {}).words || [])
    const [activeCollectionId, setActiveCollectionId] = useState((collections[0] || {}).collectionId);
    const [activeUniqueId, setActiveUniqueId] = useState(((collections[0] || {}).words || [])[0]);
    const [currentNote, setCurrentNote] = useState('');
    const [showModal, setShowModal] = useState(false);
    const [showMarkdownModal, setShowMarkdownModal] = useState(false);
    const [newCollection, setNewCollection] = useState({ collectionName: '', description: '' });

    const updateCollections = async () => {
        const data = await request(`${DOMAIN}/api/collections/all`, 'GET')
        setCollections(data);
    }

    const handleDelete = async (collectionId) => {
        await request(`${DOMAIN}/api/collections/delete`, 'POST', {
            collectionId
        })
        await updateCollections()
    };

    const handleUnCollect = async (uniqueId) => {
        const data = await request(`${DOMAIN}/api/collections/uncollect`, 'POST', {
            collectionId: activeCollectionId,
            uniqueId
        })
        if (data.code === 0) {
            await updateCollections()
        }
    };

    const handleAddCollection = async () => {
        await request(`${DOMAIN}/api/collections/add`, 'POST', {
            collectionId: encrypt(newCollection.collectionName),
            collectionName: newCollection.collectionName,
            description: newCollection.description,
        })
        await updateCollections()
        setNewCollection({ collectionName: '', description: '' });
        setShowModal(false);
    };

    const modalBody = (
        <Box sx={{ position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)', width: 400, bgcolor: 'background.paper', boxShadow: 24, p: 4 }}>
            <Typography variant="h6" component="h2">
                Add New Collection
            </Typography>
            <TextField fullWidth label="Collection Name" margin="normal" value={newCollection.collectionName} onChange={(e) => setNewCollection({ ...newCollection, collectionName: e.target.value })} />
            <TextField fullWidth label="Description" margin="normal" value={newCollection.description} onChange={(e) => setNewCollection({ ...newCollection, description: e.target.value })} />
            <Box sx={{ display: 'flex', justifyContent: 'flex-end', mt: 2 }}>
                <Button className="text-white bg-blue-500 hover:bg-blue-700" variant="contained" onClick={handleAddCollection}>Add</Button>
            </Box>
        </Box>
    );

    const handleCollectionClick = async (collectionId) => {
        console.log('handleCollectionClick')
        setActiveCollectionId(collectionId)
        const data = await request(`${DOMAIN}/api/getCollectionData`, 'POST', {
            collectionId
        })
        if(data.code === 0) {
            console.log(data)
        }
    }

    const handleWordClick = (uniqueId) => {
        setActiveUniqueId(uniqueId)
    }

    const handleNoteChange = useCallback((newNoteData) => {
        console.log(newNoteData)
        setCurrentNote(newNoteData)
    }, []);

    const handleSaveNote = async () => {
        console.log(currentNote)
        console.log(currentNote.replace(/\n/g, ''))
        const data = await request(`${DOMAIN}/api/addNote`, 'POST', {
            uniqueId: activeUniqueId,
            word: decrypt(activeUniqueId),
            content: currentNote.replace(/\n/g, '')
        })
        if(data.code === 0) {
            setShowMarkdownModal(false)
        }
    }

    useEffect(() => {
        setWords((collections.find(item => item.collectionId === activeCollectionId) || {}).words || [])
    }, [activeCollectionId, collections])

    const handleClickNote = async (uniqueId) => {
        setActiveUniqueId(uniqueId);
        async function fetchData () {
            try {
                const { note = {} } = await request(`${DOMAIN}/api/getNote`, 'POST', {
                    uniqueId
                });
                if (note) {
                    setCurrentNote(note.content);
                } else {
                    setCurrentNote('');
                }
            } catch (error) {
                console.error('请求错误', error);
            }
        }
        await fetchData()
        setShowMarkdownModal(true);
    }

    const options = {
        placeholder: "Please write note here",
        showIcons: ["code", "table"],
        styleSelectedText: false,
        tabSize: 4,
        autofocus: false,
        spellChecker: false,
    }

    const handleSave = useCallback((content) => {
        console.log(currentNote)
    }, [currentNote])

    const handleCopy = (collectionId) => {
        if (navigator.clipboard && window.isSecureContext) {
            // 如果支持剪切板 API 并且在安全上下文中
            navigator.clipboard.writeText(collectionId)
                .then(() => {
                    toast.success('Copy successful', {
                        autoClose: 1000
                    });
                })
                .catch(err => toast.error("Could not copy text"));
        } else {
            toast.error("剪切板 API 不可用")
        }
    };
    

    const markdownJSX = useMemo(() => {
        return (
            <MyMarkdownEditor initialValue={currentNote} onEditorChange={handleNoteChange}  key={1} onSave={handleSave} />
        )
    }, [showMarkdownModal])

    return (
        <Box className="px-20 pt-10 flex flex-col" style={{ height: '100vh' }}>
            <div className="flex justify-between mb-1 h-10 font-medium">
                <Box className="mr-4 pr-2.5 flex items-end justify-between" style={{ flex: '0 0 20%' }}>
                    <Button className="text-white bg-blue-500 hover:bg-blue-700" onClick={() => setShowModal(true)}>Add</Button>
                    <div>Total: {collections.length}</div>
                </Box>
                <Box className="mr-4 pr-2.5 flex items-end justify-end" style={{ flex: '0 0 30%' }}>
                    Total: {words.length}
                </Box>
                <Box style={{ flex: '0 0 50%' }}>
                    
                </Box>
            </div>
            <div className="flex-1 flex w-full" style={{ height: 'calc(100% - 40px)' }}>
                <Box className="flex h-full overflow-y-auto mr-4 pr-2.5" style={{ flex: '0 0 20%' }}>
                    <List className="w-full">
                        {collections.map((item) => {
                            const labelId = `checkbox-list-label-${item.collectionId}`;

                            return (
                                <ListItem
                                    key={item.collectionId}
                                    secondaryAction={
                                        <div>
                                            
                                            <IconButton className="mr-4" onClick={() => handleCopy(item.collectionId)} edge="end" aria-label="comments">
                                            <ContentCopyIcon />
                                        </IconButton>
                                            <IconButton onClick={() => handleDelete(item.collectionId)} edge="end" aria-label="comments">
                                            <DeleteIcon />
                                        </IconButton>
                                        </div>
                                        
                                    }
                                    disablePadding
                                    className={activeCollectionId === item.collectionId ? 'bg-slate-200' : ''}
                                    onClick={() => handleCollectionClick(item.collectionId)}
                                >
                                    <ListItemButton role={undefined} dense>
                                        <ListItemIcon>
                                            <Checkbox
                                                edge="start"
                                                tabIndex={-1}
                                                disableRipple
                                                inputProps={{ 'aria-labelledby': labelId }}
                                            />
                                        </ListItemIcon>
                                        <ListItemText id={labelId} primary={item.collectionName} />
                                    </ListItemButton>
                                </ListItem>
                            );
                        })}
                    </List>
                </Box>
                <Box className="h-full overflow-y-auto mr-4 pr-2.5" style={{ flex: '0 0 30%' }}>
                    <List>
                        {
                            words.map((item) => {
                                const labelId = `checkbox-list-label-${item.uniqueId}`;
                                return (
                                    <ListItem
                                        key={item.uniqueId}
                                        secondaryAction={
                                            <div>
                                                <IconButton className="mr-4" onClick={() => handleClickNote(item.uniqueId)} edge="end" aria-label="comments">
                                                    <EditNoteIcon />
                                                </IconButton>
                                                <IconButton className="mr-4" edge="end" aria-label="comments" component="a" href={`https://www.youdao.com/result?word=${decrypt(item.uniqueId)}&lang=en`} target="_blank" rel="noopener noreferrer">
                                                    <OpenInNewIcon />
                                                </IconButton>
                                                <IconButton onClick={() => handleUnCollect(item.uniqueId)} edge="end" aria-label="comments">
                                                    <DeleteIcon />
                                                </IconButton>
                                            </div>

                                        }
                                        className={activeUniqueId === item.uniqueId ? 'bg-slate-200' : ''}
                                        onClick={() => handleWordClick(item.uniqueId)}
                                        disablePadding
                                    >
                                        <ListItemButton role={undefined} dense>
                                            <ListItemIcon>
                                                <Checkbox
                                                    edge="start"
                                                    tabIndex={-1}
                                                    disableRipple
                                                    inputProps={{ 'aria-labelledby': labelId }}
                                                />
                                            </ListItemIcon>
                                            <ListItemText id={labelId} >
                                                {decrypt(item.uniqueId)}
                                            </ListItemText>
                                        </ListItemButton>
                                    </ListItem>
                                );
                            })
                        }
                    </List>
                </Box>
                <Box className="pt-2 overflow-y-auto pr-2.5" style={{ flex: '0 0 50%' }}>
                <div className="html-string" dangerouslySetInnerHTML={{ __html: words.find(item => item.uniqueId === activeUniqueId)?.pronounce }} />
                    <div className="html-string" dangerouslySetInnerHTML={{ __html: words.find(item => item.uniqueId === activeUniqueId)?.htmlString }} />
                </Box>
            </div>

            <Modal
                open={showModal}
                onClose={() => setShowModal(false)}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                {modalBody}
            </Modal>
            <Modal
                open={showMarkdownModal}
                onClose={() => setShowMarkdownModal(false)}
            >
                <Box sx={{ position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)', bgcolor: 'background.paper', boxShadow: 24, p: 4 }}>
                    <Typography variant="h6" component="h2">
                        Note Markdown
                    </Typography>
                    <div>
                        {markdownJSX}
                    </div>
                    <Box sx={{ display: 'flex', justifyContent: 'flex-end', mt: 2 }}>
                        <Button className="text-white bg-blue-500 hover:bg-blue-700" variant="contained" onClick={handleSaveNote}>Save</Button>
                    </Box>
                </Box>
            </Modal>
        </Box>
    );
}

export default Collections;
