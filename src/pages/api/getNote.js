import dbConnect from '../../lib/dbConnect';
import Note from '../../models/Note';
import cors from '../../middlewares/cors';

export default async function handler(req, res) {
    // 使用 CORS 中间件
    await cors(req, res);

    // 数据库连接
    await dbConnect();

    if (req.method === 'POST') {
        const { uniqueId } = req.body;

        try {
            const note = await Note.findOne({ uniqueId });
            console.log(note)
            res.status(200).json({ note });
        } catch (error) {
            res.status(500).json({ message: error.message });
        }
    } else {
        res.status(405).json({ message: 'Method Not Allowed' });
    }
}
