import dbConnect from '../../lib/dbConnect';
import Note from '../../models/Note';
import cors from '../../middlewares/cors';

export default async function handler(req, res) {
    // 使用 CORS 中间件
    await cors(req, res);

    await dbConnect();

    if (req.method === 'POST') {
        const { uniqueId, word, content } = req.body;

        try {
            // 查找并更新笔记，如果不存在则创建新笔记
            const updatedNote = await Note.findOneAndUpdate(
                { uniqueId },
                { uniqueId, word, content },
                { new: true, upsert: true, setDefaultsOnInsert: true }
            );

            res.status(200).json({updatedNote, code: 0});
        } catch (error) {
            res.status(500).json({ message: error.message });
        }
    } else {
        res.status(405).json({ message: 'Method Not Allowed' });
    }
}
