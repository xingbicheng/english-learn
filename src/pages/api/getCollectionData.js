// pages/api/getCollectionData.js
import dbConnect from '../../lib/dbConnect';
import Collection from '../../models/Collection';
import Note from '../../models/Note';
import cors from '../../middlewares/cors';
import {
    decrypt
} from '../../utils/utils';

export default async function handler(req, res) {
    // 使用 CORS 中间件
    await cors(req, res);
    await dbConnect();

    if (req.method === 'POST') {
        const {
            collectionId
        } = req.body;

        try {
            const collection = await Collection.findOne({
                collectionId: collectionId
            });
            if (!collection) {
                return res.status(404).json({
                    message: 'Collection not found'
                });
            }

            const data = await Promise.all(
                collection.words.map(async (wordItem) => {
                    const note = await Note.findOne({
                        uniqueId: wordItem.uniqueId
                    });
                    return {
                        uniqueId: wordItem.uniqueId,
                        word: decrypt(wordItem.uniqueId),
                        content: note ? note.content : '',
                        htmlString: wordItem.htmlString,
                        pronounce: wordItem.pronounce
                    };
                })
            );

            res.status(200).json({
                data,
                code: 0
            });
        } catch (error) {
            res.status(500).json({
                message: error.message
            });
        }
    } else {
        res.status(405).json({
            message: 'Method Not Allowed'
        });
    }
}
