// pages/api/someEndpoint.js
import dbConnect from '../../lib/dbConnect';

export default async function handler(req, res) {
    await dbConnect();

    // 您的 API 逻辑...
}
