import dbConnect from '../../../lib/dbConnect';
import Collection from '../../../models/Collection';
import cors from '../../../middlewares/cors'

export default async function handler(req, res) {
    // 使用 CORS 中间件
    await cors(req, res);

    await dbConnect();

    if (req.method === 'POST') {
        const { collectionId, collectionName, description } = req.body;

        try {
            // 检查是否已存在具有相同 collectionId 的收藏夹
            const existingCollection = await Collection.findOne({ collectionId });
            if (existingCollection) {
                return res.status(409).json({ message: 'Collection with this ID already exists' });
            }

            // 创建新收藏夹
            const collection = new Collection({
                collectionId,
                collectionName,
                description: description || '',  // 如果没有提供 description，则默认为空字符串
                words: [],  // 初始化空单词数组
            });
            await collection.save();

            res.status(201).json(collection);
        } catch (error) {
            console.error("Error saving collection:", error);
            res.status(500).json({ message: error.message });
        }
    } else {
        res.status(405).json({ message: 'Method Not Allowed' });
    }
}
