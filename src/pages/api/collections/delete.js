import dbConnect from '../../../lib/dbConnect';
import cors from '../../../middlewares/cors';
import Collection from '../../../models/Collection';

export default async function handler(req, res) {
    // 使用 CORS 中间件
    await cors(req, res);
    await dbConnect();

    if (req.method === 'POST') {
        const { collectionId } = req.body;

        try {
            const deletedCollection = await Collection.findOneAndDelete({ collectionId });
            if (!deletedCollection) {
                return res.status(404).json({ message: 'Collection not found' });
            }

            res.status(200).json({ message: 'Collection deleted successfully' });
        } catch (error) {
            res.status(500).json({ message: error.message });
        }
    } else {
        res.status(405).json({ message: 'Method Not Allowed' });
    }
}
