import dbConnect from '../../../lib/dbConnect';
import cors from '../../../middlewares/cors';
import Collection from "../../../models/Collection";

export default async function handler (req, res) {
    // 使用 CORS 中间件
    await cors(req, res);

    // 数据库连接
    await dbConnect();

    if (req.method === 'GET') {
        try {
            const collections = await Collection.find({}).sort({ lastUsed: -1 }); // 按最近使用时间降序排序
            res.status(200).json(collections);
        } catch (error) {
            res.status(500).json({ message: error.message });
        }
    } else {
        res.status(405).json({ message: 'Method Not Allowed' });
    }
}
