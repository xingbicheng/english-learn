import dbConnect from '../../../lib/dbConnect';
import cors from '../../../middlewares/cors';
import Collection from '../../../models/Collection';

export default async function handler (req, res) {
    // 使用 CORS 中间件
    await cors(req, res);
    await dbConnect();

    if (req.method === 'POST') {
        const { collectionId, uniqueId } = req.body;

        try {
            const collection = await Collection.findOne({ collectionId: collectionId });
            const isCollected = !!collection.words.some(item => item.uniqueId === uniqueId);
            res.status(200).json({ isCollected });
        } catch (error) {
            res.status(500).json({ message: error.message });
        }
    }
}
