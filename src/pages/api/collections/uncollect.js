// pages/api/collections/uncollect.js
import dbConnect from '../../../lib/dbConnect';
import Collection from '../../../models/Collection';
import cors from '../../../middlewares/cors'

export default async function handler(req, res) {
    // 使用 CORS 中间件
    await cors(req, res);
    await dbConnect();

    if (req.method === 'POST') {
        const { uniqueId, collectionId } = req.body;

        try {
            const collection = await Collection.findOne({ collectionId: collectionId });
            if (!collection) {
                return res.status(404).json({ message: 'Collection not found' });
            }

            collection.words = collection.words.filter(item => item.uniqueId !== uniqueId);
            collection.lastUsed = new Date(); // 更新最近使用时间
            await collection.save();

            res.status(200).json({ code: 0, message: 'Word uncollected successfully' });
        } catch (error) {
            res.status(500).json({ message: error.message });
        }
    } else {
        res.status(405).json({ message: 'Method Not Allowed' });
    }
}
