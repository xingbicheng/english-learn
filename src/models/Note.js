// models/Note.js
import mongoose from 'mongoose';

const NoteSchema = new mongoose.Schema({
    uniqueId: {
        type: String,
        required: true,
        unique: true
    },
    word: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    }
});

export default mongoose.models.Note || mongoose.model('Note', NoteSchema);
