import mongoose from 'mongoose';

const collectionSchema = new mongoose.Schema({
    collectionId: {
        type: String,
        required: true,
        unique: true
    },
    collectionName: {
        type: String,
        required: true
    },
    description: String,
    words: [{
        uniqueId: String,
        htmlString: String,
        pronounce: String
    }],
    lastUsed: {
        type: Date,
        default: Date.now // 设置默认值为当前时间
    }
});

export default mongoose.models.Collection || mongoose.model('Collection', collectionSchema);
