import axios from 'axios';
import { toast } from 'react-toastify';

export const request = async (url, method, data) => {
    try {
        const response = await axios({
            method: method,
            url: url,
            data: data
        });
        console.log('Operation successful')
        toast.success('Operation successful', {
            autoClose: 1000
        });
        return response.data;
    } catch (error) {
        // 处理错误，例如显示错误消息
        console.error('API request error:', error);
        toast.error('Operation failed: ' + error.message, {
            autoClose: 8000
        });
        throw error; // 根据需要决定是否抛出错误
    }
}
