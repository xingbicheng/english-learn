// cryptoUtils.js
import CryptoJS from 'crypto-js';

const secretKey = 'english-learn'
export const DOMAIN = 'http://localhost:8000'

export function encrypt(message) {
    // 调整密钥长度以适应AES算法
    var key = CryptoJS.enc.Utf8.parse(secretKey.padEnd(32, ' ')); // 如果密钥长度小于32，则用空格填充到32
    var iv = CryptoJS.enc.Utf8.parse('00000000000000000000000000000000'); // 固定的IV

    var encrypted = CryptoJS.AES.encrypt(message, key, {
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    });

    return encrypted.toString();
}


export function decrypt(ciphertext) {
    // 调整密钥长度以适应AES算法
    var key = CryptoJS.enc.Utf8.parse(secretKey.padEnd(32, ' ')); // 如果密钥长度小于32，则用空格填充到32
    var iv = CryptoJS.enc.Utf8.parse('00000000000000000000000000000000'); // 固定的IV

    var decrypted = CryptoJS.AES.decrypt(ciphertext, key, {
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    });

    return decrypted.toString(CryptoJS.enc.Utf8);
}