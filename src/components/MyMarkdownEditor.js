import React, { useState, useEffect } from 'react';
import dynamic from 'next/dynamic';
import "easymde/dist/easymde.min.css";
import { Editor } from '@tinymce/tinymce-react';

// 动态导入SimpleMDE，关闭SSR
const SimpleMDE = dynamic(
    () => import('react-simplemde-editor'),
    { ssr: false }
);

function MyMarkdownEditor ({ onSave, ...props }) {
    useEffect(() => {
        const handleKeyDown = (event) => {
            if ((event.ctrlKey || event.metaKey) && event.key === 's') {
                event.preventDefault();
                onSave(); // 调用您的保存函数
            }
        };

        document.addEventListener('keydown', handleKeyDown);
        return () => {
            document.removeEventListener('keydown', handleKeyDown);
        };
    }, [onSave]);

    return (<Editor
        {...props}
        apiKey='a5stjqb2010e1c1oqw5p9tyjl447fxvx3vf7srdjqg3fmyue'
        init={{
            height: 500,
            width: 900,
            menubar: false,
            plugins: [
                'advlist', 'autolink', 'lists', 'link', 'image', 'charmap', 'preview',
                'anchor', 'searchreplace', 'visualblocks', 'code', 'fullscreen',
                'insertdatetime', 'media', 'table', 'code', 'help', 'wordcount'
            ],
            toolbar: 'undo redo | blocks | ' +
                'bold italic forecolor backcolor | alignleft aligncenter ' +
                'alignright alignjustify | bullist numlist outdent indent | ' +
                'removeformat | help',
            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',
        }}
    />);
}

export default MyMarkdownEditor;
