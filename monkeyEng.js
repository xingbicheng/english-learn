// ==UserScript==
// @name         YouDao add Markdown note
// @namespace    http://tampermonkey.net/
// @version      2024-01-19
// @description  try to take over the world!
// @author       You
// @match        https://www.youdao.com/result*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=youdao.com
// @grant        none
// ==/UserScript==

(function () {
    'use strict';
    console.log('youdao');

    const secretKey = 'english-learn'

    function showSuccessMessage (message) {
        Toastify({
            text: message,
            duration: 8000,
            close: true,
            gravity: "top",
            position: "right",
            backgroundColor: "#4CAF50",
        }).showToast();
    }

    function showErrorMessage (message) {
        Toastify({
            text: message,
            duration: 8000,
            close: true,
            gravity: "top",
            position: "right",
            backgroundColor: "#FF5733",
        }).showToast();
    }

    function handleSaveClick () {
        const {
            word,
            uniqueId
        } = getWordAndUniqueId()
        // 获取 Markdown 内容
        // const markdownContent = simplemde.value();
        const markdownContent = tinymce.get('editor').getContent()

        // 发送到 addNote 接口
        addNoteToServer(uniqueId, word, markdownContent);
    }

    function addNoteToServer (uniqueId, word, content) {
        fetch('http://localhost:8000/api/addNote', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                uniqueId,
                word,
                content: content.replace(/\n/g, '')
            })
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then(data => {
                showSuccessMessage("Note saved successfully");
            })
            .catch(error => {
                showErrorMessage("Error saving note: " + error.message);
            });
    }

    function getNoteFromServer (uniqueId) {
        fetch(`http://localhost:8000/api/getNote`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                uniqueId,
            })
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Failed to fetch note');
                }
                return response.json();
            })
            .then(data => {
                if (data && data.note) {
                    tinymce.get('editor').setContent(data.note.content);
                    // simplemde.value(data.note.content);
                } else {
                    tinymce.get('editor').setContent(''); // 如果没有笔记内容，清空编辑器
                    // simplemde.value(''); // 如果没有笔记内容，清空编辑器
                }
            })
            .catch(error => {
                showErrorMessage("Error fetching note: " + error.message);
                // simplemde.value(''); // 请求失败时，清空编辑器
                tinymce.get('editor').setContent(''); // 请求失败时，清空编辑器
            });
    }

    function encrypt (message) {
        // 调整密钥长度以适应AES算法
        var key = CryptoJS.enc.Utf8.parse(secretKey.padEnd(32, ' ')); // 如果密钥长度小于32，则用空格填充到32
        var iv = CryptoJS.enc.Utf8.parse('00000000000000000000000000000000'); // 固定的IV

        var encrypted = CryptoJS.AES.encrypt(message, key, {
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });

        return encrypted.toString();
    }


    function decrypt (ciphertext) {
        // 调整密钥长度以适应AES算法
        var key = CryptoJS.enc.Utf8.parse(secretKey.padEnd(32, ' ')); // 如果密钥长度小于32，则用空格填充到32
        var iv = CryptoJS.enc.Utf8.parse('00000000000000000000000000000000'); // 固定的IV

        var decrypted = CryptoJS.AES.decrypt(ciphertext, key, {
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });

        return decrypted.toString(CryptoJS.enc.Utf8);
    }

    // 隐藏不需要的元素
    function hideElements () {
        const elementsToHide = ['.dict_indexes-con', '.user-feed_back'];
        elementsToHide.forEach(selector => {
            const element = document.querySelector(selector);
            if (element) {
                element.style.display = 'none';
            }
        });
    };

    // 调整布局样式
    function adjustLayout () {
        const searchResult = document.querySelector('.search_result');
        if (searchResult) {
            searchResult.style.cssText = 'margin-left: 0; margin-right: 0; padding: 0 80px; box-sizing: border-box; width: 100%; display: flex;';
            const mainResult = document.querySelector('.search_result-main');
            if (mainResult) {
                mainResult.style.flex = '1';
                mainResult.style.marginRight = '40px';
            }
            const dictResult = document.querySelector('.search_result-dict');
            if (dictResult) {
                dictResult.style.width = '100%';
            }
        }
    };

    function initTinyMCE () {
        const editorDiv = document.createElement('div');
        createFavoriteDropdown(editorDiv);
        const textDiv = document.createElement('textarea');
        textDiv.id = 'editor'
        editorDiv.appendChild(textDiv);
        editorDiv.style.flex = '1';
        editorDiv.id = 'editor-container'
        document.querySelector('.search_result').appendChild(editorDiv);
        tinymce.init({
            selector: '#editor',
            menubar: false,
            plugins: [
                'advlist', 'autolink', 'lists', 'link', 'image', 'charmap', 'preview',
                'anchor', 'searchreplace', 'visualblocks', 'code', 'fullscreen',
                'insertdatetime', 'media', 'table', 'code', 'help', 'wordcount'
            ],
            toolbar: 'undo redo | blocks | ' +
                'bold italic forecolor backcolor | alignleft aligncenter ' +
                'alignright alignjustify | bullist numlist outdent indent | ' +
                'removeformat | help',
            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',
            setup: function (editor) {
                editor.on('init', function () {
                    // 现在可以安全地设置内容了
                    fetchAndDisplayNote()
                });
                editor.on('keydown', function(event) {
                    // 检查是否按下了 Command+S (对于 macOS) 或 Ctrl+S (对于 Windows/Linux)
                    if ((event.ctrlKey || event.metaKey) && event.keyCode === 83) {
                        // 阻止默认的保存行为
                        event.preventDefault();
                        // 执行保存函数
                        handleSaveClick();
                        return false; // 阻止事件进一步传播
                    }
                });
            }
        });
    };

    // 监听 URL 变化
    let lastUrl = window.location.href;
    setInterval(() => {
        let currentUrl = window.location.href;
        if (currentUrl !== lastUrl) {
            lastUrl = currentUrl;
            fetchAndDisplayNote(); // 获取新 URL 的笔记
            updateDropdown();
        }
    }, 500); // 检查频率可以根据需要调整

    function handleLoadLink (src) {
        const link = document.createElement('link');
        link.rel = 'stylesheet';
        link.href = src;
        document.head.appendChild(link);
    }

    function handleLoadScript (src, onloadCallback) {
        const script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = src;
        document.head.appendChild(script);
        if (onloadCallback) {
            script.onload = onloadCallback
        }
    }

    // 加载 SimpleMDE 资源
    const loadSimpleMDE = () => {
        handleLoadScript('https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/crypto-js.min.js', () => {
            handleLoadLink('https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css')
            handleLoadScript('https://code.jquery.com/jquery-3.6.0.min.js', () => {
                handleLoadScript('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/umd/popper.min.js', () => {
                    handleLoadScript('https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', () => {

                    })
                })
            })
            handleLoadLink('https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css')
            handleLoadLink('https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css')
            handleLoadScript('https://cdn.tiny.cloud/1/a5stjqb2010e1c1oqw5p9tyjl447fxvx3vf7srdjqg3fmyue/tinymce/6/tinymce.min.js', () => {
                initTinyMCE()
                // 获取并显示当前 URL 的笔记
                // fetchAndDisplayNote();
            })
            handleLoadScript('https://cdn.jsdelivr.net/npm/toastify-js')
        })
    };

    function fetchAndDisplayNote () {
        const {
            word,
            uniqueId
        } = getWordAndUniqueId()
        if (word) {
            getNoteFromServer(uniqueId);
        }
    }

    const handleClickDropdownItem = (checkbox, uniqueId, collection) => {
        const selectedCollection = localStorage.getItem('selectedCollection');
        const isCollected = checkbox.checked;
        if (isCollected) {
            uncollectWord(uniqueId, collection.collectionId)
                .then(() => {
                    showSuccessMessage("uncollect");
                    checkbox.checked = false; // 更新复选框状态
                    if (!!selectedCollection && selectedCollection === collection.collectionId) {
                        setMainBtnCollectStatus(false);
                    }
                })
                .catch(error => {
                    showErrorMessage("Error uncollect:" + error.message);
                    return false; // 发生错误时返回空数组
                });
        } else {
            collectWord(uniqueId, collection.collectionId)
                .then(() => {
                    showSuccessMessage("collect");
                    checkbox.checked = true; // 更新复选框状态
                    if (!!selectedCollection && selectedCollection === collection.collectionId) {
                        setMainBtnCollectStatus(true);
                    }
                })
                .catch(error => {
                    showErrorMessage("Error collect:" + error.message);
                    return false; // 发生错误时返回空数组
                });
        }

    }

    function handleClickMainBtn () {
        const {
            word,
            uniqueId
        } = getWordAndUniqueId();
        const mainBtn = document.querySelector('#mainBtn')
        // 从 localStorage 获取当前选中的 collection
        const selectedCollection = localStorage.getItem('selectedCollection');
        if (!selectedCollection) {
            showErrorMessage("Error: please select default collection!");
        } else {
            if (mainBtn.innerHTML === 'Collect') {
                collectWord(uniqueId, selectedCollection)
                    .then(() => {
                        showSuccessMessage("collect");
                        updateDropdown()
                    })
                    .catch(error => {
                        showErrorMessage("Error collect:" + error.message);
                        return false; // 发生错误时返回空数组
                    });
            } else {
                uncollectWord(uniqueId, selectedCollection)
                    .then(() => {
                        showSuccessMessage("uncollect");
                        updateDropdown()
                    })
                    .catch(error => {
                        showErrorMessage("Error uncollect:" + error.message);
                        return false; // 发生错误时返回空数组
                    });
            }
        }
    }



    function createFavoriteDropdown (editorDiv) {
        const {
            word,
            uniqueId
        } = getWordAndUniqueId();
        const operateElement = editorDiv;
        if (!operateElement) return;

        // 创建包含按钮和下拉的容器
        const btnGroup = document.createElement('div');
        btnGroup.className = 'btn-group';

        const rowElement = document.createElement('div');
        rowElement.style.marginBottom = '10px';
        operateElement.appendChild(rowElement);

        // 在这里添加 Save Note 按钮
        const saveNoteButton = document.createElement('button');
        saveNoteButton.className = 'btn btn-info';  // Bootstrap 'info' 蓝色按钮
        saveNoteButton.textContent = 'Save Note';
        saveNoteButton.style.marginLeft = '10px';  // 添加一些右边距
        saveNoteButton.addEventListener('click', handleSaveClick);

        // 创建 Bootstrap select 框
        const selectBox = document.createElement('select');
        selectBox.innerHTML = '<option value="" disabled selected>Select default collection</option>'; // 默认提示选项
        selectBox.className = 'custom-select'; // 使用 Bootstrap 的 custom-select 类
        selectBox.style.marginLeft = '10px'; // 添加一些右边距
        selectBox.style.width = '250px';

        // 从 localStorage 获取当前选中的 collection
        const selectedCollection = localStorage.getItem('selectedCollection');

        rowElement.appendChild(btnGroup);  // 然后添加 btnGroup
        rowElement.appendChild(saveNoteButton);  // 将 Save Note 按钮添加到 rowElement
        rowElement.appendChild(selectBox);  // 将 Select Box 添加到 rowElement

        // 创建主按钮
        const mainButton = document.createElement('button');
        mainButton.id = 'mainBtn';
        mainButton.className = 'btn btn-primary';
        mainButton.innerHTML = 'Collect';
        mainButton.addEventListener('click', handleClickMainBtn);

        btnGroup.appendChild(mainButton);

        if (!!selectedCollection) {
            checkCollectionStatus(selectedCollection).then(isCollected => {
                if (isCollected) {
                    setMainBtnCollectStatus(true)
                } else {
                    setMainBtnCollectStatus(false)
                }
            });
        }

        // 创建下拉触发按钮
        const dropdownButton = document.createElement('button');
        dropdownButton.className = 'btn btn-primary dropdown-toggle dropdown-toggle-split';
        dropdownButton.setAttribute('data-toggle', 'dropdown');
        dropdownButton.setAttribute('aria-haspopup', 'true');
        dropdownButton.setAttribute('aria-expanded', 'false');
        const spanForCaret = document.createElement('span');
        spanForCaret.className = 'sr-only';
        spanForCaret.textContent = 'Toggle Dropdown';
        dropdownButton.appendChild(spanForCaret);
        btnGroup.appendChild(dropdownButton);

        // 创建下拉菜单内容
        const dropdownMenu = document.createElement('div');
        dropdownMenu.className = 'dropdown-menu';
        btnGroup.appendChild(dropdownMenu);

        // 为 select 框添加 change 事件监听器
        selectBox.addEventListener('change', function () {
            // 将选择存储到 localStorage
            localStorage.setItem('selectedCollection', this.value);
            checkCollectionStatus(this.value).then(isCollected => {
                console.log(isCollected)
                if (isCollected) {
                    setMainBtnCollectStatus(true)
                } else {
                    setMainBtnCollectStatus(false)
                }
            });
        });

        // 添加收藏夹项目到下拉菜单
        getCollectionData().then(collections => {

            collections.forEach(collection => {
                const option = document.createElement('option');
                option.value = collection.collectionId;
                option.textContent = collection.collectionName;
                selectBox.appendChild(option);

                // 设置 select 框的初始选中值
                if (collection.collectionId === selectedCollection) {
                    selectBox.value = selectedCollection;
                }

                const menuItem = document.createElement('a');
                menuItem.className = 'dropdown-item';
                const checkbox = document.createElement('input');
                checkbox.type = 'checkbox';
                menuItem.appendChild(checkbox);
                menuItem.append(` ${collection.collectionName}`);
                dropdownMenu.appendChild(menuItem);

                // 为每个下拉菜单项添加点击事件监听器
                menuItem.addEventListener('click', () => handleClickDropdownItem(checkbox, uniqueId, collection));

                checkCollectionStatus(collection.collectionId).then(isCollected => {
                    checkbox.checked = isCollected;
                });
            });

            // 如果 localStorage 中没有 selectedCollection，则保持 select 框的初始状态，即没有任何项被选中
            if (!selectedCollection) {
                selectBox.value = ""; // 设置 select 框为默认提示选项
            }


        });
    }

    function setMainBtnCollectStatus (isCollected) {
        const mainButton = document.querySelector('#mainBtn')
        console.log(isCollected, mainButton)
        if (isCollected) {
            mainButton.className = 'btn btn-danger';
            mainButton.innerHTML = 'Uncollect';
        } else {
            mainButton.className = 'btn btn-primary';
            mainButton.innerHTML = 'Collect';
        }
        console.log(mainButton)
    }

    function updateDropdown () {
        const {
            word,
            uniqueId
        } = getWordAndUniqueId()

        // 从 localStorage 获取当前选中的 collection
        const selectedCollection = localStorage.getItem('selectedCollection');

        if (!!selectedCollection) {
            // 更新复选框状态
            checkCollectionStatus(selectedCollection).then(isCollected => {
                if (isCollected) {
                    setMainBtnCollectStatus(true)
                } else {
                    setMainBtnCollectStatus(false)
                }
            });
        }

        getCollectionData().then(collections => {
            const dropdownMenu = document.querySelector('.dropdown-menu');
            const mainButton = document.querySelector('#mainBtn')
            console.log(mainButton)
            dropdownMenu.innerHTML = ''; // 清空当前下拉菜单

            collections.forEach(collection => {
                const menuItem = document.createElement('a');
                menuItem.className = 'dropdown-item';
                const checkbox = document.createElement('input');
                checkbox.type = 'checkbox';
                menuItem.appendChild(checkbox);
                menuItem.append(` ${collection.collectionName}`);
                dropdownMenu.appendChild(menuItem);

                // 为每个下拉菜单项添加点击事件监听器
                menuItem.addEventListener('click', () => handleClickDropdownItem(checkbox, uniqueId, collection));

                // 更新复选框状态
                checkCollectionStatus(collection.collectionId).then(isCollected => {
                    checkbox.checked = isCollected;
                });
            });
        });
    }

    function getCollectionData () {
        return fetch('http://localhost:8000/api/collections/all')
            .then(response => response.json())
            .then(data => {
                return data;
            })
            .catch(error => {
                showErrorMessage("Error fetching collections:" + error.message);
                return []; // 发生错误时返回空数组
            });
    }

    function getWordAndUniqueId () {
        const urlParams = new URLSearchParams(window.location.search);
        const word = urlParams.get('word');
        const uniqueId = encrypt(word);
        return {
            word,
            uniqueId,
        }
    }

    function checkCollectionStatus (collectionId) {
        const {
            word,
            uniqueId,
        } = getWordAndUniqueId();
        return fetch('http://localhost:8000/api/collections/checkIsCollected', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                collectionId,
                uniqueId,
            })
        }).then(response => response.json())
            .then(data => data.isCollected)
            .catch(error => {
                showErrorMessage("Error checkCollectionStatus:" + error.message);
                return false; // 发生错误时返回空数组
            });
    }

    function collectWord (uniqueId, collectionId) {
        return fetch('http://localhost:8000/api/collections/collect', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                uniqueId,
                collectionId,
                htmlString: getStyledHtml('.simple.dict-module'),
                pronounce: getStyledHtml('.phone_con')
            })
        });
    }

    function uncollectWord (uniqueId, collectionId) {
        return fetch('http://localhost:8000/api/collections/uncollect', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                uniqueId,
                collectionId
            })
        });
    }

    function getStyledHtml (selector) {
        var element = document.querySelector(selector);
        if (!element) {
            return ''; // 元素不存在时返回空字符串
        }

        return element.outerHTML.replace(/\sdata-v-\w+="[^"]*"/g, '');
    }

    // 初始化函数或在脚本的适当位置
    function init () {
        
    }



    // 执行初始化
    init();
    // 执行脚本
    loadSimpleMDE();
    hideElements();
    adjustLayout();
})();